package com.example.ralphcarneiro.qrcode.dao;

import java.util.List;

/**
 * Created by gefersondias on 11/06/17.
 */

public interface DAO <T>{
    void insert(T novo);
    void delete(Long id);
    void deleteAll();
    void update(T novo);
    T get(long id);
    List<T> getList();

}
