package com.example.ralphcarneiro.qrcode.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.example.ralphcarneiro.qrcode.model.ChekinInfo;
import com.example.ralphcarneiro.qrcode.model.DataCheckin;
import com.example.ralphcarneiro.qrcode.model.Minicurso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 11/06/2017.
 */

public class CheckinDAO implements  DAO<DataCheckin> {

    private SQLiteDatabase banco;
    private static final String TABELA_REGISTRO = "checkin";


    public CheckinDAO(Context context) {
        banco = new BancoHelper(context).getWritableDatabase();
    }

    @Override
    public void insert(DataCheckin novo) {

        ContentValues cv = new ContentValues();
        cv.put("cpf_congressista", novo.getCpf());
        cv.put("id_evento", novo.getId_evento());
        cv.put("hora_checkin", novo.getHora_checkin());
        banco.insert(TABELA_REGISTRO, null, cv);

    }

    @Override
    public void delete(Long id) {

        this.banco.delete(TABELA_REGISTRO, "id = ?", new String[]{Long.toString(id)});

    }

    @Override
    public void deleteAll() {
        this.banco.delete(TABELA_REGISTRO, null, null);
    }

    @Override
    public void update(DataCheckin novo) {

        ContentValues cv = new ContentValues();
        cv.put("cpf_congressista", novo.getCpf());
        cv.put("id_evento", novo.getId_evento());
        cv.put("hora_checkin", novo.getHora_checkin());
        this.banco.update(TABELA_REGISTRO, cv, "id = ?", new String[]{Long.toString(novo.getId())});

    }

    @Override
    public DataCheckin get(long id) {

        String []colunas = {"id","cpf_congressista","id_evento","hora_checkin"};
        String tabela = TABELA_REGISTRO;
        Cursor c = this.banco.query(tabela,colunas,"id = ?", new String[]{Long.toString(id)},null,null,null);
        DataCheckin l;

        if(c.moveToNext()){

            l = new DataCheckin();
            l.setId(c.getLong(c.getColumnIndex("id")));
            l.setCpf(c.getString(c.getColumnIndex("cpf_congressista")));
            l.setId_evento(c.getLong(c.getColumnIndex("id_evento")));
            l.setHora_checkin(c.getString(c.getColumnIndex("hora_checkin")));

            return l;
        }
        throw new RuntimeException(String.format("Video %d não existe", id));
    }

    @Override
    public List<DataCheckin> getList() {
        String []colunas = {"id","cpf_congressista","id_evento","hora_checkin"};
        String tabela = TABELA_REGISTRO;
        Cursor c = this.banco.query(tabela,colunas,null,null,null,null,null);
        DataCheckin l;
        List<DataCheckin> lista = new ArrayList<DataCheckin>();
        while(c.moveToNext()){
            l = new DataCheckin();
            l.setId(c.getLong(c.getColumnIndex("id")));
            l.setCpf(c.getString(c.getColumnIndex("cpf_congressista")));
            l.setId_evento(c.getLong(c.getColumnIndex("id_evento")));
            l.setHora_checkin(c.getString(c.getColumnIndex("hora_checkin")));
            lista.add(l);

        }

        return lista;
    }
    public long count(){
        return DatabaseUtils.queryNumEntries(this.banco, TABELA_REGISTRO);
    }
    public SQLiteDatabase getBanco(){
        return this.banco;
    }
}
