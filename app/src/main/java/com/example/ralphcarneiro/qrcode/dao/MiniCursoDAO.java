package com.example.ralphcarneiro.qrcode.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.example.ralphcarneiro.qrcode.model.Minicurso;
import com.example.ralphcarneiro.qrcode.model.Palestra;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 11/06/2017.
 */

public class MiniCursoDAO implements  DAO<Minicurso> {

        private SQLiteDatabase banco;
        private static final String TABELA_REGISTRO = "minicurso";


        public MiniCursoDAO(Context context) {
            banco = new BancoHelper(context).getWritableDatabase();
        }

        @Override
        public void insert(Minicurso novo) {

            ContentValues cv = new ContentValues();
            cv.put("id", novo.getId());
            cv.put("nome_minicurso", novo.getNomeMinicurso());
            cv.put("descricao_minicurso", novo.getDescricaoMinucurso());
            cv.put("data_minicurso", novo.getDate());
            cv.put("palestrante", novo.getNomePalestrante());
            banco.insert(TABELA_REGISTRO, null, cv);

        }

        @Override
        public void delete(Long id) {

            this.banco.delete(TABELA_REGISTRO, "id = ?", new String[]{Long.toString(id)});

        }

    @Override
    public void deleteAll() {
        this.banco.delete(TABELA_REGISTRO, null, null);
    }

    @Override
        public void update(Minicurso novo) {

            ContentValues cv = new ContentValues();
            cv.put("nome_minicurso", novo.getNomeMinicurso());
            cv.put("descricao_minicurso", novo.getDescricaoMinucurso());
            cv.put("data_minicurso", novo.getDate());
            cv.put("palestrante", novo.getNomePalestrante());
            this.banco.update(TABELA_REGISTRO, cv, "id = ?", new String[]{Long.toString(novo.getId())});

        }

        @Override
        public Minicurso get(long id) {

            String []colunas = {"id","nome_minicurso","descricao_minicurso","data_minicurso", "palestrante"};
            String tabela = TABELA_REGISTRO;
            Cursor c = this.banco.query(tabela,colunas,"id = ?", new String[]{Long.toString(id)},null,null,null);
            Minicurso l;

            if(c.moveToNext()){

                l = new Minicurso();
                l.setId(c.getInt(c.getColumnIndex("id")));
                l.setNomeMinicurso(c.getString(c.getColumnIndex("nome_minicurso")));
                l.setDescricaoMinucurso(c.getString(c.getColumnIndex("descricao_minicurso")));
                l.setDate(c.getString(c.getColumnIndex("data_minicurso")));
                l.setNomePalestrante(c.getString(c.getColumnIndex("palestrante")));

                return l;
            }
            throw new RuntimeException(String.format("Video %d não existe", id));
        }

    public long count(){
        return DatabaseUtils.queryNumEntries(this.banco, TABELA_REGISTRO);
    }

        @Override
        public List<Minicurso> getList() {
            String []colunas = {"id","nome_minicurso","descricao_minicurso","data_minicurso", "palestrante"};
            String tabela = TABELA_REGISTRO;
            Cursor c = this.banco.query(tabela,colunas,null,null,null,null,null);
            Minicurso l;
            List<Minicurso> lista = new ArrayList<Minicurso>();
            while(c.moveToNext()){
                l = new Minicurso();
                l.setId(c.getInt(c.getColumnIndex("id")));
                l.setNomeMinicurso(c.getString(c.getColumnIndex("nome_minicurso")));
                l.setDescricaoMinucurso(c.getString(c.getColumnIndex("descricao_minicurso")));
                l.setDate(c.getString(c.getColumnIndex("data_minicurso")));
                l.setNomePalestrante(c.getString(c.getColumnIndex("palestrante")));
                lista.add(l);

            }

            return lista;
        }

        public SQLiteDatabase getBanco(){
            return this.banco;
        }
}
