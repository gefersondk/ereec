package com.example.ralphcarneiro.qrcode.util;

import com.example.ralphcarneiro.qrcode.applicationEreec.Login;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by Ralph Carneiro on 02/06/2017.
 */

public class LoginDes implements JsonDeserializer<Object> {
    @Override
    public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonElement login = json.getAsJsonObject();

        if(json.getAsJsonObject().get("login") != null){
            login = json.getAsJsonObject().get("login");
        }
        return (new Gson().fromJson(login, Login.class));
    }
}
