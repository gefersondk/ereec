package com.example.ralphcarneiro.qrcode.applicationEreec;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ralphcarneiro.qrcode.R;
import com.example.ralphcarneiro.qrcode.dao.CheckinDAO;
import com.example.ralphcarneiro.qrcode.model.ChekinInfo;
import com.example.ralphcarneiro.qrcode.model.DataCheckin;
import com.example.ralphcarneiro.qrcode.model.StatusResponse;
import com.example.ralphcarneiro.qrcode.service.APIService;
import com.example.ralphcarneiro.qrcode.service.EreecRetrofitServiceConfig;
import com.example.ralphcarneiro.qrcode.util.EreecConstants;
import com.example.ralphcarneiro.qrcode.util.MacUtil;
import com.example.ralphcarneiro.qrcode.util.NetWorkUtil;

import java.net.NetworkInterface;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Confirmar_Checkin extends AppCompatActivity {

    @Bind(R.id.dataCheckin)
    protected TextView dataCheckin;
    @Bind(R.id.horaCheckin)
    protected TextView horaCheckin;
    @Bind(R.id.nomeCongressista)
    protected TextView nomeCongressista;
    @Bind(R.id.CPF)
    protected TextView cpf;
    @Bind(R.id.btn_confirm_checkin)
    protected Button btn_confirm_checkin;
    private ChekinInfo chekinInfo;
    private Context context;

    APIService service;
    DataCheckin dataCheckinObj;
    CheckinDAO checkinDAO;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmar__checkin);
        context = getApplicationContext();
        checkinDAO = new CheckinDAO(context);
        ButterKnife.bind(this);



        Intent intent = getIntent();
        if(intent != null && intent.hasExtra("chekinInfo")){
            DateFormat dft = new SimpleDateFormat("HH:mm", Locale.ROOT);
            DateFormat dfd = new SimpleDateFormat("dd/MM/yyyy", Locale.ROOT);

            chekinInfo = intent.getParcelableExtra("chekinInfo");
            cpf.setText(chekinInfo.getCpf());
            nomeCongressista.setText(chekinInfo.getNome());
            String hour = dft.format(Calendar.getInstance().getTime());
            horaCheckin.setText(hour);
            String date = dfd.format(Calendar.getInstance().getTime());
            dataCheckin.setText(date);

            dataCheckinObj = new DataCheckin(chekinInfo.getCpf(), chekinInfo.getId_evento(), new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ROOT).format(new Date()));
        }


        btn_confirm_checkin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (dataCheckinObj != null) {
                        if(!NetWorkUtil.isConnected(context)){
                            checkinDAO.insert(dataCheckinObj);
                            alerta(Confirmar_Checkin.this, "Sem conexão: Checkin adicionado à lista de espera");
                        }else {
                            if (service == null)
                                service = EreecRetrofitServiceConfig.getServiceConfig();
                            String mac = MacUtil.getMacAddr((WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE));
                            final Call<StatusResponse> checkinCall = service.checkinCongressista(mac, dataCheckinObj);

                            checkinCall.enqueue(new Callback<StatusResponse>() {
                                @Override
                                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                                    StatusResponse statusResponse = response.body();
                                    if (response.isSuccessful()) {
                                        if (statusResponse.getSuccess()) {
                                            setResult(EreecConstants.RESULT_CONFIRM_CHECKIN);
                                            alerta(Confirmar_Checkin.this, "Checkin realizado com sucesso!");
                                        } else {
                                            alerta(Confirmar_Checkin.this, "Não foi possível realizar o checkin com as informações fornecidas!");

                                        }
                                    } else{
                                        Toast.makeText(getApplicationContext(), "Erro no servidor", Toast.LENGTH_LONG).show();
                                        checkinDAO.insert(dataCheckinObj);
                                    }

                                }

                                @Override
                                public void onFailure(Call<StatusResponse> call, Throwable t) {
                                    Log.i("Login", t.getMessage());
                                }
                            });
                        }
                    }
                    }
        });
    }

    public void alerta(Activity activity, String message) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle("Checkin");
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    finish();
                }
            });

        builder.show();

    }

    @Override
    protected void onDestroy() {
        checkinDAO.getBanco().close();
        super.onDestroy();
    }
}
