package com.example.ralphcarneiro.qrcode.service;

import com.example.ralphcarneiro.qrcode.util.EreecConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by gefersondias on 04/06/17.
 */

public class EreecRetrofitServiceConfig {
    public static APIService getServiceConfig(){
//yyyy-MM-dd'T'HH:mm:ssZ
//        Gson gson = new GsonBuilder()
//                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
//                .create();
        //inicio TODO remover quando for para produção
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        //fim
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EreecConstants.Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())//TODO remover quando for para produção
                .build();
        return retrofit.create(APIService.class);
    }
}
