package com.example.ralphcarneiro.qrcode.applicationEreec;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.ralphcarneiro.qrcode.R;
import com.example.ralphcarneiro.qrcode.dao.CheckinDAO;
import com.example.ralphcarneiro.qrcode.model.DataCheckin;
import com.example.ralphcarneiro.qrcode.model.Palestra;
import com.example.ralphcarneiro.qrcode.model.StatusResponse;
import com.example.ralphcarneiro.qrcode.service.APIService;
import com.example.ralphcarneiro.qrcode.service.EreecRetrofitServiceConfig;
import com.example.ralphcarneiro.qrcode.util.EreecConstants;
import com.example.ralphcarneiro.qrcode.util.MacUtil;
import com.example.ralphcarneiro.qrcode.util.NetWorkUtil;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Alex on 11/06/2017.
 */

public class CheckinPedente extends AppCompatActivity {

    private CheckinDAO checkinDAO;
    private Context context;
    @Bind(R.id.btn_confirm_pedente)
    protected Button btn_checkin_pendente;
    @Bind(R.id.btn_confirm_pedente_cancel)
    protected Button btn_cancel_checkin_pendente;

    ProgressDialog progress;

    APIService service;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkin_pedente);
        ButterKnife.bind(this);

        context = getApplicationContext();
        
        checkinDAO = new CheckinDAO(context);

        btn_cancel_checkin_pendente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btn_checkin_pendente.setOnClickListener(new RegisterCheckin());

    }

    public class RegisterCheckin implements View.OnClickListener{

        @Override
        public void onClick(View v) {

            if(checkinDAO.count() > 0){

                progress = ProgressDialog.show(CheckinPedente.this, "Realizando Chekins", getString(R.string.message_dialog_autentication), true);

                List<DataCheckin> dataCheckins = checkinDAO.getList();

//                for (DataCheckin dataCheckin: dataCheckins) {
//                    registerCheckin(dataCheckin);
//                }
                new AsyncTask<List<DataCheckin>, Void, Void>(){

                    @Override
                    protected Void doInBackground(List<DataCheckin>... lists) {
                        for (DataCheckin dataCheckin: lists[0]) {
                            registerCheckin(dataCheckin);
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        progress.dismiss();
                        checkinDAO.getBanco().close();
                        finish();
                    }
                }.execute(dataCheckins);


            }

        }
    }

    public void registerCheckin(final DataCheckin dataCheckinObj){


        if(!NetWorkUtil.isConnected(context)){
            progress.dismiss();
        }else {
            if (service == null)
                service = EreecRetrofitServiceConfig.getServiceConfig();
            String mac = MacUtil.getMacAddr((WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE));
            final Call<StatusResponse> checkinCall = service.checkinCongressista(mac, dataCheckinObj);

            checkinCall.enqueue(new Callback<StatusResponse>() {
                @Override
                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                    StatusResponse statusResponse = response.body();
                    if (response.isSuccessful()) {
                        if (statusResponse.getSuccess()) {
                            checkinDAO.delete(dataCheckinObj.getId());
                        } else {
                        }
                    } else
                        Toast.makeText(getApplicationContext(), "Erro no servidor", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(Call<StatusResponse> call, Throwable t) {
                    Log.i("Login", t.getMessage());
                }
            });
        }

    }
}
