package com.example.ralphcarneiro.qrcode.EreecAdapter;

import android.view.View;

/**
 * Created by gefersondias on 03/06/17.
 */

public interface RecyclerViewOnClickListenerContact {

    void onClickListener(View view, int position);
    void onLongPressClickListener(View view, int position);
}
