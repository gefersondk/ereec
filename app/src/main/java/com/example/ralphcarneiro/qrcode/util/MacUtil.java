package com.example.ralphcarneiro.qrcode.util;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Base64;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

/**
 * Created by Alex on 06/06/2017.
 */

public class MacUtil {
    public static String getMacAddr(WifiManager activityManager) {
        /*if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            WifiInfo info = activityManager.getConnectionInfo();
            return info.getMacAddress().replace("\n", "");
        }else*/ try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:",b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString().replace("\n", "");
            }
        } catch (Exception ignored) {}
        return "02:00:00:00:00:00";
    }
}
