package com.example.ralphcarneiro.qrcode.model;

/**
 * Created by Alex on 06/06/2017.
 */

public class DataCheckin {
    private long id;
    private String cpf;
    private long id_evento;
    private String hora_checkin;

    public DataCheckin(String cpf, long id_evento, String hora_checkin) {
        this.cpf = cpf;
        this.id_evento = id_evento;
        this.hora_checkin = hora_checkin;
    }

    public DataCheckin() {
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public long getId_evento() {
        return id_evento;
    }

    public void setId_evento(long id_evento) {
        this.id_evento = id_evento;
    }

    public long getId() {return id;}

    public void setId(long id) {this.id = id;}

    public String getHora_checkin() {return hora_checkin;}

    public void setHora_checkin(String hora_checkin) {this.hora_checkin = hora_checkin;}
}
