package com.example.ralphcarneiro.qrcode.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Alex on 08/06/2017.
 */

public class ChekinInfo implements Parcelable {

    public ChekinInfo() {
    }

    public ChekinInfo(String nome, String cpf, long id_evento) {
        this.nome = nome;
        this.cpf = cpf;
        this.id_evento = id_evento;
    }

    private String nome;
    private String cpf;
    private long id_evento;

    protected ChekinInfo(Parcel in) {
        nome = in.readString();
        cpf = in.readString();
        id_evento = in.readLong();
    }

    public static final Creator<ChekinInfo> CREATOR = new Creator<ChekinInfo>() {
        @Override
        public ChekinInfo createFromParcel(Parcel in) {
            return new ChekinInfo(in);
        }

        @Override
        public ChekinInfo[] newArray(int size) {
            return new ChekinInfo[size];
        }
    };

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public long getId_evento() {
        return id_evento;
    }

    public void setId_evento(long id_evento) {
        this.id_evento = id_evento;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nome);
        dest.writeString(cpf);
        dest.writeLong(id_evento);
    }
}
