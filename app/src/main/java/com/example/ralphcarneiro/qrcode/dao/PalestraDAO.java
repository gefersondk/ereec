package com.example.ralphcarneiro.qrcode.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.example.ralphcarneiro.qrcode.model.Palestra;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 11/06/2017.
 */

public class PalestraDAO implements  DAO<Palestra> {

        private SQLiteDatabase banco;
        private static final String TABELA_REGISTRO = "palestra";


        public PalestraDAO(Context context) {
            banco = new BancoHelper(context).getWritableDatabase();
        }

        @Override
        public void insert(Palestra novo) {

            ContentValues cv = new ContentValues();
            cv.put("id", novo.getId());
            cv.put("nome_palestra", novo.getNomePalestra());
            cv.put("descricao_palestra", novo.getDescricaoPalestra());
            cv.put("data_palestra", novo.getDate());
            cv.put("palestrante", novo.getNomePalestrante());
            banco.insert(TABELA_REGISTRO, null, cv);

        }

        @Override
        public void delete(Long id) {

            this.banco.delete(TABELA_REGISTRO, "id = ?", new String[]{Long.toString(id)});

        }

    @Override
    public void deleteAll() {
        this.banco.delete(TABELA_REGISTRO, null, null);
    }

        @Override
        public void update(Palestra novo) {

            ContentValues cv = new ContentValues();
            cv.put("nome_palestra", novo.getNomePalestra());
            cv.put("descricao_palestra", novo.getDescricaoPalestra());
            cv.put("data_palestra", novo.getDate());
            cv.put("palestrante", novo.getNomePalestrante());
            this.banco.update(TABELA_REGISTRO, cv, "id = ?", new String[]{Long.toString(novo.getId())});

        }

        @Override
        public Palestra get(long id) {

            String []colunas = {"id","nome_palestra","descricao_palestra","data_palestra", "palestrante"};
            String tabela = TABELA_REGISTRO;
            Cursor c = this.banco.query(tabela,colunas,"id = ?", new String[]{Long.toString(id)},null,null,null);
            Palestra l;

            if(c.moveToNext()){

                l = new Palestra();
                l.setId(c.getInt(c.getColumnIndex("id")));
                l.setNomePalestra(c.getString(c.getColumnIndex("nome_palestra")));
                l.setDescricaoPalestra(c.getString(c.getColumnIndex("descricao_palestra")));
                l.setDate(c.getString(c.getColumnIndex("data_palestra")));
                l.setNomePalestrante(c.getString(c.getColumnIndex("palestrante")));

                return l;
            }
            throw new RuntimeException(String.format("Video %d não existe", id));
        }

        @Override
        public List<Palestra> getList() {
            String []colunas = {"id","nome_palestra","descricao_palestra","data_palestra", "palestrante"};
            String tabela = TABELA_REGISTRO;
            Cursor c = this.banco.query(tabela,colunas,null,null,null,null,null);
            Palestra l;
            List<Palestra> lista = new ArrayList<Palestra>();
            while(c.moveToNext()){
                l = new Palestra();
                l.setId(c.getInt(c.getColumnIndex("id")));
                l.setNomePalestra(c.getString(c.getColumnIndex("nome_palestra")));
                l.setDescricaoPalestra(c.getString(c.getColumnIndex("descricao_palestra")));
                l.setDate(c.getString(c.getColumnIndex("data_palestra")));
                l.setNomePalestrante(c.getString(c.getColumnIndex("palestrante")));
                lista.add(l);

            }

            return lista;
        }

        public long count(){
            return DatabaseUtils.queryNumEntries(this.banco, TABELA_REGISTRO);
        }

        public SQLiteDatabase getBanco(){
            return this.banco;
        }
}
