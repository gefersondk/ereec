package com.example.ralphcarneiro.qrcode.model;

/**
 * Created by Alex on 06/06/2017.
 */

public class StatusResponse {
    private String message;
    private Boolean success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
