package com.example.ralphcarneiro.qrcode.util;

/**
 * Created by gefersondias on 04/06/17.
 */

public class EreecConstants {

    public static final int RESULT_CONFIRM_CHECKIN = 7;
    public static final int RESULT_SCAN = 555;
    public static final int RESULT_CONFIRM_CHECKIN_CANCELL = 333;
    public static final int PERMISSIONS_REQUEST_ACCESS_CAMERA = 0;
    public static final String EREEC_CHECK = "ereec_check_update";
    public class Api{

        public static final String BASE_URL = "http://192.168.15.2:8080/api/"; //"http://192.168.15.2:8080/api/"; //"https://ereecnne2017.com.br/api/";
        public static final String HOST = "http://ereecnne2017.com.br";

    }

}
