package com.example.ralphcarneiro.qrcode.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mateu on 28/05/2017.
 */

public class Palestra implements Parcelable {

    @SerializedName("id")
    private long id;
    @SerializedName("nome")
    private String nomePalestra;
    @SerializedName("palestrante")
    private String nomePalestrante;
    @SerializedName("descricao")
    private String descricaoPalestra;
    @SerializedName("data")
    private String date;

    public Palestra(){}

    public Palestra(long id, String nomePalestra, String nomePalestrante, String descricaoPalestra, String date) {
        this.id = id;
        this.nomePalestra = nomePalestra;
        this.nomePalestrante = nomePalestrante;
        this.descricaoPalestra = descricaoPalestra;
        this.date = date;

    }

    protected Palestra(Parcel in) {
        id = in.readLong();
        nomePalestra = in.readString();
        nomePalestrante = in.readString();
        descricaoPalestra = in.readString();
        date = in.readString();
    }

    public static final Creator<Palestra> CREATOR = new Creator<Palestra>() {
        @Override
        public Palestra createFromParcel(Parcel in) {
            return new Palestra(in);
        }

        @Override
        public Palestra[] newArray(int size) {
            return new Palestra[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNomePalestra() {
        return nomePalestra;
    }

    public String getNomePalestrante() {
        return nomePalestrante;
    }

    public String getDate() {
        return date;
    }

    public void setNomePalestra(String nomePalestra) {
        this.nomePalestra = nomePalestra;
    }

    public void setNomePalestrante(String nomePalestrante) {
        this.nomePalestrante = nomePalestrante;
    }

    public String getDescricaoPalestra() {
        return descricaoPalestra;
    }

    public void setDescricaoPalestra(String descricaoPalestra) {
        this.descricaoPalestra = descricaoPalestra;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Palestra{" +
                "id=" + id +
                ", nomePalestra='" + nomePalestra + '\'' +
                ", nomePalestrante='" + nomePalestrante + '\'' +
                ", descricaoPalestra='" + descricaoPalestra + '\'' +
                ", date='" + date + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(nomePalestra);
        dest.writeString(nomePalestrante);
        dest.writeString(descricaoPalestra);
        dest.writeString(date);
    }
}