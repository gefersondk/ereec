package com.example.ralphcarneiro.qrcode.EreecFragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.ralphcarneiro.qrcode.EreecAdapter.EreecCardAdapterMinecursos;
import com.example.ralphcarneiro.qrcode.EreecAdapter.RecyclerViewOnClickListenerContact;
import com.example.ralphcarneiro.qrcode.R;
import com.example.ralphcarneiro.qrcode.applicationEreec.ItemMinicurso;
import com.example.ralphcarneiro.qrcode.dao.MiniCursoDAO;
import com.example.ralphcarneiro.qrcode.dao.PalestraDAO;
import com.example.ralphcarneiro.qrcode.model.Minicurso;
import com.example.ralphcarneiro.qrcode.model.Palestra;
import com.example.ralphcarneiro.qrcode.service.APIService;
import com.example.ralphcarneiro.qrcode.service.EreecRetrofitServiceConfig;
import com.example.ralphcarneiro.qrcode.util.EreecConstants;
import com.example.ralphcarneiro.qrcode.util.MacUtil;
import com.example.ralphcarneiro.qrcode.util.NetWorkUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gefersondias on 03/06/17.
 */

public class EreecFragmentMinicursos extends Fragment implements RecyclerViewOnClickListenerContact {

    @Bind(R.id.card_list)
    protected RecyclerView recList;

    @Bind(R.id.swipeRefreshLayout)
    protected SwipeRefreshLayout swipeRefreshLayoutMinicursos;

    @Bind(R.id.progressbar_minicurso)
    protected ProgressBar progressBar;

    private List<Minicurso> ereecList;

    private EreecCardAdapterMinecursos adapter;
    private APIService service;

    private MiniCursoDAO miniCursoDAO;

    private SharedPreferences sharedpreferences_cheackupdate;

    private boolean updated;
    private Context context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();

        miniCursoDAO = new MiniCursoDAO(context);
        sharedpreferences_cheackupdate = context.getSharedPreferences(EreecConstants.EREEC_CHECK, Context.MODE_PRIVATE);
        String check = sharedpreferences_cheackupdate.getString("last_updated_curso", "");
        SimpleDateFormat dataFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ROOT);
        if(check.trim().equals("")){
            SharedPreferences.Editor editor = sharedpreferences_cheackupdate.edit();
            editor.putString("last_updated_curso", dataFormat.format(new Date()));
            editor.apply();
            updated = true;
        }else{

            try {
                Date data_check = dataFormat.parse(check);
                Date data_atual = new Date();

                float diff = (((data_atual.getTime() - data_check.getTime()) / 3600000.0f) % 24f);

                updated = diff > 2;

            } catch (ParseException e) {

            }

        }

        ereecList = new ArrayList<Minicurso>();
        adapter = new EreecCardAdapterMinecursos(getContext(), ereecList, getActivity(), false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ereec_minicursos, container, false);
        ButterKnife.bind(this, view);

        LinearLayoutManager ll = new LinearLayoutManager(getActivity());
        ll.setOrientation(LinearLayoutManager.VERTICAL);
        //ll.setReverseLayout(true);
        //recList.setHasFixedSize(false);
        recList.setLayoutManager(ll);//lm
        recList.addOnItemTouchListener(new EreecFragmentMinicursos.RecyclerViewTouchListener(getActivity(),recList, this));

        recList.setAdapter(adapter);
        swipeRefreshLayoutMinicursos.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));



        return view;
    }


    @Override
    public void onClickListener(View view, int position) {
        String url = "";
        try {
            Intent intent = new Intent(getActivity(), ItemMinicurso.class);
            intent.putExtra("evento", ereecList.get(position));
            startActivity(intent);
            /*if(palestraList != null) {
                url = palestraList.get(position).getSlug();
                Intent intent = new Intent(getActivity(), VDF_activity_produtos.class);
                intent.putExtra("slug", palestraList.get(position).getSlug());
                startActivity(intent);
            }*/
        } catch (Exception e) {

        }

    }

    @Override
    public void onLongPressClickListener(View view, int position) {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        loadMinicursos();

        swipeRefreshLayoutMinicursos.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        loadMinicursos();
                    }
                });
            }
        });
    }

    public void loadMinicursos() {
//        for(int i = 0; i < 15; i++){
//            ereecList.add(new Minicurso());
//        }
//        adapter.addAll(ereecList);
        if(!NetWorkUtil.isConnected(context)){
            ereecList = miniCursoDAO.getList();
            adapter.addAll(ereecList);
            progressBar.setVisibility(View.GONE);
//            showDialog(context.getString(R.string.erro_conexao), context.getString(R.string.message_dialog_erro_conection), true, false);
        }else {
            try {

                if (service == null)
                    service = EreecRetrofitServiceConfig.getServiceConfig();


                String mac = MacUtil.getMacAddr((WifiManager) getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE));
                Call<List<Minicurso>> dataEventCall = service.loadMiniCurso(mac);
                dataEventCall.enqueue(new Callback<List<Minicurso>>() {
                    @Override
                    public void onResponse(Call<List<Minicurso>> call, Response<List<Minicurso>> response) {
                        final List<Minicurso> minicursos = response.body();

                        if (minicursos != null) {
                            ereecList = minicursos;

                            adapter.addAll(ereecList);//adiciona a lista

                            progressBar.setVisibility(View.GONE);

                            if (updated || miniCursoDAO.count() <= 0) {
                                insertPalestra(minicursos);
                            }
                        }
                        swipeRefreshLayoutMinicursos.setRefreshing(false);
                    }

                    @Override
                    public void onFailure(Call<List<Minicurso>> call, Throwable t) {

                    }


                });

            } catch (Exception e) {

            }
        }
    }

    private static class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener{
        private Context context;
        private GestureDetector gestureDetector;
        RecyclerViewOnClickListenerContact recyclerViewOnClickListenerContact;

        public RecyclerViewTouchListener(Context c, final RecyclerView rv, RecyclerViewOnClickListenerContact rVOnCLContact){
            this.context = c;
            this.recyclerViewOnClickListenerContact = rVOnCLContact;
            this.gestureDetector = new GestureDetector(c, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                    View v = rv.findChildViewUnder(e.getX(),e.getY());
                    if(v != null && recyclerViewOnClickListenerContact != null){
                        recyclerViewOnClickListenerContact.onLongPressClickListener(v, rv.getChildPosition(v));//pegar posição da view
                    }
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    View v = rv.findChildViewUnder(e.getX(),e.getY());
                    if(v != null && recyclerViewOnClickListenerContact != null){
                        recyclerViewOnClickListenerContact.onClickListener(v, rv.getChildPosition(v));//pegar posição da view
                    }

                    return true;
                }
            });

        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            gestureDetector.onTouchEvent(e);
            //Toast.makeText(context, "onInterceptTouchEvent",Toast.LENGTH_SHORT).show();
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            //Toast.makeText(context, "onTouchEvent",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            //Toast.makeText(context, "onRequestDisallowInterceptTouchEvent",Toast.LENGTH_SHORT).show();
        }

    }

    public void insertPalestra(List<Minicurso> miniCursos){
        miniCursoDAO.deleteAll();
        for (Minicurso minicurso: miniCursos) {
            miniCursoDAO.insert(minicurso);
        }
        updateSharedP();
    }

    public void updateSharedP(){
        SharedPreferences.Editor editor = sharedpreferences_cheackupdate.edit();
        SimpleDateFormat dataFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ROOT);
        editor.putString("last_updated_curso", dataFormat.format(new Date()));
        editor.apply();
    }

    @Override
    public void onDestroy() {
        miniCursoDAO.getBanco().close();
        super.onDestroy();
    }
}
