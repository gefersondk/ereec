package com.example.ralphcarneiro.qrcode.service;

import com.example.ralphcarneiro.qrcode.model.Autenticar;
import com.example.ralphcarneiro.qrcode.model.DataCheckin;
import com.example.ralphcarneiro.qrcode.model.Minicurso;
import com.example.ralphcarneiro.qrcode.model.Palestra;
import com.example.ralphcarneiro.qrcode.model.StatusResponse;
import com.example.ralphcarneiro.qrcode.model.Usuario;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;


/**
 * Created by gefersondias on 04/06/17.
 */

public interface APIService {
    @POST("auth/mobile")
    Call<Usuario> login(@Header("Authorization") String mac, @Body Autenticar autenticar);

    @GET("list/palestra")
    Call<List<Palestra>> loadPalestras(@Header("Authorization") String mac);

    @GET("list/curso")
    Call<List<Minicurso>> loadMiniCurso(@Header("Authorization") String mac);

    @POST("checkin/congressita")
    Call<StatusResponse> checkinCongressista(@Header("Authorization") String mac, @Body DataCheckin checkin);

    /*@GET("categories")
    Call<List<VDFcategoria>> loadCategories();
    //Call<List<DataObjectVDF>> loadCategories();

    @GET("categories/{slug}")
    Call<VDFProdutos_Categorias> loadCategorieProduct(@Path("slug") String slug);

    @GET("categories/{slug_categorie}/products/{slug_product}")
    Call<Product> loadProdutct(@Path("slug_categorie") String slug, @Path("slug_product") String id);

    @POST("clients")
    Call<ResponseBody> createUser(@Body Data user);

    @PUT("clients/update")
    Call<ResponseBody> updateUser(@Body Data user);

    @POST("client_sessions")
    Call<VDFauth> authentication(@Body StartSession session);

    @HTTP(method = "DELETE", path = "client_sessions/signout", hasBody = true)//"client_sessions/signout"
    Call<ResponseBody> logout(@Header("Authorization") String authorization, @Body Auth_Token auth_token);

    @POST("orders")
    Call<ResponseBody> budgetRequest(@Header("Authorization") String authorization, @Body DataOrder session);

    @GET("orders")
    Call<List<Order>> loadBudgets(@Header("Authorization") String authorization);

    @POST("device_registrations")
    Call<ResponseBody> device_registration(@Header("Authorization") String authorization, @Body Device_Token device_token);

    @POST("orders/{id}/resend")
    Call<ResponseBody> budget_resend(@Header("Authorization") String authorization, @Path("id") String id);

    @DELETE("orders/{id}")
    Call<ResponseBody> del_order(@Header("Authorization") String authorization, @Path("id") String id);

    //@Multipart
    //@PUT("clients/avatar")
    //Call<ResponseBody> upload_image(@Header("Authorization") String authorization, @Part("avatar\"; filename=\"image.jpg") RequestBody  file);//MultipartBody.Part

    @GET("clients/me")
    Call<VDFuser> load_profile(@Header("Authorization") String authorization);

    @PUT("clients/update")
    Call<VDFuser> update_profile(@Header("Authorization") String authorization, @Body Data data);

    //@POST("conversations")
    //@Call<>()
    @POST("conversations")
    Call<Conversations> create_conversations(@Header("Authorization") String authorization, @Body Createconversations data);

    @POST("conversations/{id}/messages")
    Call<ResponseBody> send_message(@Header("Authorization") String authorization, @Path("id") int id,@Body MessageSend data);

    @GET("conversations")
    Call<List<Conversations>> list_conversations(@Header("Authorization") String authorization);

    @DELETE("conversations/{id}")
    Call<ResponseBody> del_conversations(@Header("Authorization") String authorization, @Path("id") int id);


    @Multipart
    @PUT("clients/avatar")
    Call<ResponseBody> upload_image(@Header("Authorization") String authorization, @Part MultipartBody.Part body);

    //conversations/1/messages
    @GET("conversations/{id}/messages")
    Call<List<Messages>> list_messages_user_sup(@Header("Authorization") String authorization, @Path("id") int id);

    @POST("reset_password")
    Call<ResponseBody> reset_password(@Body Recover_Password email);
    /*@GET("events")
    Call<DataEvent> loadEvents();

    //@GET("events")
    //Call<DataEvent> loadNextEvents(@Query("page[number]") String numPagina,@Query("page[size]") String size ); //só teria que passar o interio  pra esses dois parametros  humcom o numero da pagina que tu quer e a quantidade de dados
    @GET("events")
    Call<DataEvent> loadNextEvents(@Query("page[number]") int numPagina, @Query("page[size]") int size);

    @GET("events")
    Call<DataEvent> loadCityEvents(@Query("search[city]") String city);

    @GET("events")
    Call<DataEvent> loadStateEvents(@Query("search[state]") String state);

    @POST("users")
    Call<Data> createUser(@Body VDFuser user);

    @POST("sessions")
    Call<String> login(String authentication_token);

    @DELETE("sessions")
    Call<String> logout(String authentication_token);*/
}

