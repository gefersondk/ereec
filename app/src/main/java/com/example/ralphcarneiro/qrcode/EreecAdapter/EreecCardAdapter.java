package com.example.ralphcarneiro.qrcode.EreecAdapter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ralphcarneiro.qrcode.R;
import com.example.ralphcarneiro.qrcode.model.Palestra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EreecCardAdapter extends RecyclerView.Adapter<EreecCardAdapter.ViewHolderEreec>{

    ContentValues values;

    private List<Palestra> palestrasList;
    private LayoutInflater inflater;
    public static Context context;
    private static Activity activity;
    private Typeface robotoLight;
    private Typeface robotoRegular;
    private Typeface robotoBold;
    private boolean animation;
    private Map backgroundMap;
    //private float scale;
    //private int width;
    //private int height;

    public EreecCardAdapter(Context context, List<Palestra> ereecList, Activity activity, boolean animation){
        EreecCardAdapter.context = context;
        this.palestrasList = ereecList;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        EreecCardAdapter.activity = activity;

        this.animation = animation;
        this.backgroundMap = new HashMap(){};




    }

    @Override
    public ViewHolderEreec onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_palestra_minicurso, parent, false);//parent é o recyclerview. false pra usar o layout params do recyclerview
        ViewHolderEreec myViewHolderVDF=  new ViewHolderEreec(view);
        return myViewHolderVDF;
    }

    @Override
    public void onBindViewHolder(ViewHolderEreec holder, int position) {
        Palestra palestra = palestrasList.get(position);
        holder.name.setText(palestra.getNomePalestra());
        holder.palestrante.setText(palestra.getNomePalestrante());
        holder.data.setText(palestra.getDate());

    }

    @Override
    public int getItemCount() {
        return palestrasList.size();
    }

    public List<Palestra> getList(){
        return this.palestrasList;
    }

    public void addListItem(Palestra dataObject, int position){
        palestrasList.add(dataObject);
        notifyItemInserted(position);
    }

    public void removeListItem(int position){
        palestrasList.remove(position);
        notifyItemRemoved(position);
    }

    public void addAll(List<Palestra> palestras){

        ArrayList<Palestra> list_aux = new ArrayList<>();
        for(Palestra p: palestras){
            list_aux.add(p);
        }
        this.palestrasList.clear();
        this.palestrasList.addAll(list_aux);
        notifyDataSetChanged();
    }

    public static class ViewHolderEreec extends RecyclerView.ViewHolder{

        //@Bind(R.id.thumbnail_event)
//        public SimpleDraweeView photo;



        @Bind(R.id.palestra_holder_palestra)
        public TextView name;
        @Bind(R.id.palestra_holder_palestrante)
        public TextView palestrante;
        @Bind(R.id.palestra_holder_data)
        public TextView data;



        public ViewHolderEreec(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    protected class PalestraClick implements View.OnClickListener{

        protected String title, description, url, img_url;

        public PalestraClick(String title, String description, String url, String img_url){

            this.title = title;
            this.description = description;
            this.url = url;
            this.img_url = img_url;

        }

        @Override
        public void onClick(View v) {

        }
    }
}
