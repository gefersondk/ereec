package com.example.ralphcarneiro.qrcode.applicationEreec;

/**
 * Created by gefersondias on 04/06/17.
 */

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.ralphcarneiro.qrcode.R;
import com.example.ralphcarneiro.qrcode.model.ChekinInfo;
import com.example.ralphcarneiro.qrcode.model.Minicurso;
import com.example.ralphcarneiro.qrcode.util.EreecConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.zxing.Result;

import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by gefersondias on 04/06/17.
 */

public class ItemMinicurso extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    //private ZXingScannerView mScannerView;
    @Bind(R.id.nomeMinicurso)
    protected TextView nome_minicurso;
    @Bind(R.id.dataMinicurso)
    protected TextView data_minicurso;
    @Bind(R.id.descricaoMinicurso)
    protected TextView descrica_minicurso;
    @Bind(R.id.palestrante)
    protected TextView palestrante;
    @Bind(R.id.btn_start_checkin)
    protected Button iniciarCheckin;
    private Context context;
    private Minicurso evento;

    TextView textViewPalestra;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minicurso);
        ButterKnife.bind(this);

        context = getApplicationContext();

        Bundle extras = getIntent().getExtras();


        if (extras != null) {
            evento = extras.getParcelable("evento");
            nome_minicurso.setText(evento != null ? evento.getNomeMinicurso() : "");
            palestrante.setText(evento != null ? evento.getNomePalestrante() : "");
            data_minicurso.setText(evento != null ? evento.getDate() : "");
            descrica_minicurso.setText(evento != null ? evento.getDescricaoMinucurso() : "");
        }
        //com.example.ralphcarneiro.qrcode.model.Palestra palesta = (com.example.ralphcarneiro.qrcode.model.Palestra) bundle.getSerializable("palestra");

        iniciarCheckin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startScan();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == EreecConstants.RESULT_SCAN) {

            String contents = intent.getStringExtra("SCAN_RESULT");
            validarCheckin(contents);
        }else{
            if(resultCode != EreecConstants.RESULT_CONFIRM_CHECKIN && resultCode != EreecConstants.RESULT_CONFIRM_CHECKIN_CANCELL){
                startScan();
            }
        }
    }

    public void startScan(){
        Intent intent = new Intent(this, ErrecScannerActivity.class);
        startActivityForResult(intent, EreecConstants.RESULT_SCAN);
    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    public void validarCheckin (String result){
        ChekinInfo chekinInfo = checkResult(result);
        if (chekinInfo != null){
            chekinInfo.setId_evento(evento.getId());
            Intent intent = new Intent(getBaseContext(), Confirmar_Checkin.class);
            intent.putExtra("chekinInfo", chekinInfo);
            startActivityForResult(intent , EreecConstants.RESULT_CONFIRM_CHECKIN);
        }else
            alerta(ItemMinicurso.this);
    }

    public void handleResult (Result result){
        ChekinInfo chekinInfo = checkResult(result.getText());
        if (chekinInfo != null){
            chekinInfo.setId_evento(evento.getId());
            Intent intent = new Intent(getBaseContext(), Confirmar_Checkin.class);
            intent.putExtra("chekinInfo", chekinInfo);
            startActivityForResult(intent , EreecConstants.RESULT_CONFIRM_CHECKIN);
        }else
            alerta(ItemMinicurso.this);
    }

    public ChekinInfo checkResult(String result){
        try{
            Gson gson = new GsonBuilder().create();
            ChekinInfo chekinInfo = gson.fromJson(result, ChekinInfo.class);
            if (chekinInfo != null && Pattern.matches("^[0-9]{3}[.][0-9]{3}[.][0-9]{3}[-][0-9]{2}", chekinInfo.getCpf()))
                return chekinInfo;
        }catch (JsonSyntaxException ignored){}

        return null;
    }

    public void alerta(Activity activity) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("QRCODE");
        builder.setMessage("Não foi possivel identificar o QR");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        builder.show();
    }

}