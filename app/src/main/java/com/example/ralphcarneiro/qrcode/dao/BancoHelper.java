package com.example.ralphcarneiro.qrcode.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by gefersondias on 11/06/17.
 */

public class BancoHelper extends SQLiteOpenHelper {

    private static final int VERSAO = 1;

    public BancoHelper(Context context) {
        super(context, "ereecdb", null, VERSAO);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        StringBuilder palestra = new StringBuilder();
        StringBuilder minicurco = new StringBuilder();
        StringBuilder checkin = new StringBuilder();

        palestra.append("CREATE TABLE [palestra] ([id] INTEGER NOT NULL PRIMARY KEY, [nome_palestra] VARCHAR NOT NULL, [descricao_palestra] VARCHAR NOT NULL, [data_palestra] VARCHAR NOT NULL, [palestrante] VARCHAR NOT NULL);");

        db.execSQL(palestra.toString());

        minicurco.append("CREATE TABLE [minicurso] ([id] INTEGER NOT NULL PRIMARY KEY, [nome_minicurso] VARCHAR NOT NULL, [descricao_minicurso] VARCHAR NOT NULL, [data_minicurso] VARCHAR NOT NULL, [palestrante] VARCHAR NOT NULL);");

        db.execSQL(minicurco.toString());

        checkin.append("CREATE TABLE [checkin] ([id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, [cpf_congressista] VARCHAR NOT NULL, [id_evento] VARCHAR NOT NULL, [hora_checkin] VARCHAR NOT NULL);");
        db.execSQL(checkin.toString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try{
            db.execSQL("drop table palestra");
            db.execSQL("drop table minicurso");
            db.execSQL("drop table checkin");
        }
        catch (Exception e){
            System.out.println("errro ao criar");
        }

        onCreate(db);

    }

}
