package com.example.ralphcarneiro.qrcode.EreecAdapter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ralphcarneiro.qrcode.R;
import com.example.ralphcarneiro.qrcode.model.Minicurso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by gefersondias on 03/06/17.
 */


public class EreecCardAdapterMinecursos extends RecyclerView.Adapter<EreecCardAdapterMinecursos.ViewHolderEreec>{

    ContentValues values;

    private List<Minicurso> minicursosLit;
    private LayoutInflater inflater;
    public static Context context;
    private static Activity activity;
    private Typeface robotoLight;
    private Typeface robotoRegular;
    private Typeface robotoBold;
    private boolean animation;
    private Map backgroundMap;
    //private float scale;
    //private int width;
    //private int height;

    public EreecCardAdapterMinecursos(Context context, List<Minicurso> ereecList, Activity activity, boolean animation){
        EreecCardAdapterMinecursos.context = context;
        this.minicursosLit = ereecList;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        EreecCardAdapterMinecursos.activity = activity;
        this.animation = animation;
        this.backgroundMap = new HashMap(){};




    }

    @Override
    public ViewHolderEreec onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_palestra_minicurso, parent, false);//parent é o recyclerview. false pra usar o layout params do recyclerview
        ViewHolderEreec myViewHolderVDF=  new ViewHolderEreec(view);
        return myViewHolderVDF;
    }

    @Override
    public void onBindViewHolder(ViewHolderEreec holder, int position) {
        Random r = new Random();
        int p = r.nextInt(5);
        Minicurso minicurso = minicursosLit.get(position);
        holder.name.setText(minicurso.getNomeMinicurso());
        holder.palestrante.setText(minicurso.getNomePalestrante());
        holder.data.setText(minicurso.getDate());
    }

    @Override
    public int getItemCount() {
        return minicursosLit.size();
    }

    public List<Minicurso> getList(){
        return this.minicursosLit;
    }

    public void addListItem(Minicurso dataObject, int position){
        minicursosLit.add(dataObject);
        notifyItemInserted(position);
    }

    public void removeListItem(int position){
        minicursosLit.remove(position);
        notifyItemRemoved(position);
    }

    public void addAll(List<Minicurso> minucursos){

        ArrayList<Minicurso> list_aux = new ArrayList<>();
        for(Minicurso m: minucursos){
            list_aux.add(m);
        }
        this.minicursosLit.clear();
        this.minicursosLit.addAll(list_aux);
        notifyDataSetChanged();
    }

    public static class ViewHolderEreec extends RecyclerView.ViewHolder{


        @Bind(R.id.view_span_m7)
        protected View view_line;


        @Bind(R.id.palestra_holder_palestra)
        public TextView name;
        @Bind(R.id.palestra_holder_palestrante)
        public TextView palestrante;
        @Bind(R.id.palestra_holder_data)
        public TextView data;


        public ViewHolderEreec(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            view_line.setBackgroundResource(R.color.green);
        }
    }

    protected class MinicursoClick implements View.OnClickListener{

        protected String title, description, url, img_url;

        public MinicursoClick(String title, String description, String url, String img_url){

            this.title = title;
            this.description = description;
            this.url = url;
            this.img_url = img_url;

        }

        @Override
        public void onClick(View v) {

        }
    }
}

