package com.example.ralphcarneiro.qrcode.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ralph Carneiro on 06/06/2017.
 */

public class Autenticar {
    @SerializedName("username")
    private String email;
    @SerializedName("password")
    private String senha;

    public Autenticar(String email, String senha) {
        this.email = email;
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
