package com.example.ralphcarneiro.qrcode.applicationEreec;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.example.ralphcarneiro.qrcode.EreecFragment.EreecFragment;
import com.example.ralphcarneiro.qrcode.EreecFragment.EreecFragmentPalestras;
import com.example.ralphcarneiro.qrcode.R;
import com.example.ralphcarneiro.qrcode.dao.CheckinDAO;
import com.example.ralphcarneiro.qrcode.dao.PalestraDAO;
import com.example.ralphcarneiro.qrcode.model.Usuario;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import butterknife.Bind;
import butterknife.ButterKnife;


public class EreecMainActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    private Context context;
    private Drawer result;
    private AccountHeader headerResult;
    private EreecFragment fragmentSwitch;
    private Fragment fragment;
    public IProfile profile;
    @Bind(R.id.toolbar)
    protected Toolbar toolbar;
    private FragmentManager fragmentManager;
    private PalestraDAO palestraDAO;
    NetworkStateReceiver networkStateReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ereec_main_activity);

        ButterKnife.bind(this);

        context = getApplicationContext();

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        palestraDAO = new PalestraDAO(context);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        this.fragmentManager = getSupportFragmentManager();
        if(savedInstanceState == null){

            this.fragment = new EreecFragmentPalestras();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
        }

        Bundle bundle = getIntent().getExtras();
        Usuario usuario = bundle.getParcelable("usuario");
        profile = new ProfileDrawerItem().withIdentifier(101).withName(usuario != null ? usuario.getNome() : "").withTextColorRes(R.color.orange).withEmail(usuario != null ? usuario.getEmail() : "").withIcon(getResources().getDrawable(R.drawable.profile2));//profile2

        // Create the AccountHeader
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.side_nav_bar)
                .withProfileImagesVisible(false)
                .withSelectionListEnabledForSingleProfile(false)
                //.withHeaderBackground(R.color.orange)
                .addProfiles(
                        profile
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profileU, boolean currentProfile) {

                        return false;
                    }
                })
                .build();


        result = new DrawerBuilder()
                .withActivity(this)
                .withSliderBackgroundColorRes(R.color.md_white_1000)
                .withToolbar(toolbar)
                .withActionBarDrawerToggleAnimated(true)
                .withSavedInstance(savedInstanceState)
                .withAccountHeader(headerResult)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.palestra).withTextColorRes(R.color.black).withIdentifier(0).withIcon(R.mipmap.ic_list_black_24dp).withTag("palestras"),
                        new PrimaryDrawerItem().withName(R.string.minicursos).withTextColorRes(R.color.black).withIdentifier(1).withIcon(R.mipmap.ic_list_black_24dp).withTag("minicursos")
                ).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if(drawerItem.getTag() != null && !drawerItem.getTag().equals("logout"))
                            replaceFragment(drawerItem.getTag().toString());
                        if(drawerItem.getTag().equals("logout")){
                            alerta(EreecMainActivity.this);
                        }


                        return false;
                    }
                }).withOnDrawerListener(new Drawer.OnDrawerListener() {

                    @Override
                    public void onDrawerOpened(View drawerView) {

                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {

                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {

                    }
                })
                .build();

//        result.setSelection(0);
        result.addStickyFooterItem(
                new PrimaryDrawerItem().withName(R.string.logout).withTextColorRes(R.color.md_white_1000).withIcon(R.mipmap.ic_power_settings_new_black_24dp).withTag("logout")
        );

        Log.d("verificando", "created Activity");
        result.getStickyFooter().setBackgroundColor(getResources().getColor(R.color.primary));

    }

    public void replaceFragment(String tag){
        fragmentSwitch = EreecFragment.valueOf(tag);
        fragment = fragmentSwitch.getEreecFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }

    public void alerta(Activity activity) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Logout");
        builder.setMessage("Você tem certeza que deseja sair!");

        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivity(intent);
                finish();
            }
        });

        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        builder.show();
    }

    public void insertEnvent(){

    }

    @Override
    protected void onStart() {
//        if (networkChangeReceiver == null){
//            IntentFilter intentFilter = new IntentFilter();
//            intentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
//            intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
//            intentFilter.addAction("android.net.wifi.STATE_CHANGE");
//            networkChangeReceiver = new NetworkChangeReceiver();
//            registerReceiver(networkChangeReceiver, intentFilter);
//        }
        super.onStart();
    }

    @Override
    protected void onRestart() {
        if (networkStateReceiver == null){
            networkStateReceiver = new NetworkStateReceiver();
        }
        if(networkStateReceiver.getSizeListener() == 0) {
            networkStateReceiver.addListener(this);
            this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        }
        super.onRestart();
    }

    @Override
    protected void onPause() {
//        if (networkChangeReceiver != null) {
//            unregisterReceiver(networkChangeReceiver);
//            networkChangeReceiver = null;
//        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (networkStateReceiver != null) {
            networkStateReceiver.removeListener(this);
            unregisterReceiver(networkStateReceiver);
            networkStateReceiver = null;
        }
        super.onDestroy();
    }

    @Override
    public void networkAvailable() {
        CheckinDAO checkinDAO = new CheckinDAO(context);
        if (checkinDAO.count() > 0) {
            startActivity(new Intent(context, CheckinPedente.class));
        }
        checkinDAO.getBanco().close();
    }

    @Override
    public void networkUnavailable() {

    }
}
