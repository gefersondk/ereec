package com.example.ralphcarneiro.qrcode.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ralph Carneiro on 06/06/2017.
 */

public class Usuario implements Parcelable{
    private String email;
    private String nome;
    private Boolean success;

    public Usuario(String email, String nome, Boolean success) {
        this.email = email;
        this.nome = nome;
        this.success = success;
    }

    protected Usuario(Parcel in) {
        email = in.readString();
        nome = in.readString();
    }

    public static final Creator<Usuario> CREATOR = new Creator<Usuario>() {
        @Override
        public Usuario createFromParcel(Parcel in) {
            return new Usuario(in);
        }

        @Override
        public Usuario[] newArray(int size) {
            return new Usuario[size];
        }
    };

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeString(nome);
    }
}