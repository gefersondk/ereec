package com.example.ralphcarneiro.qrcode.applicationEreec;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ralphcarneiro.qrcode.R;
import com.example.ralphcarneiro.qrcode.model.Autenticar;
import com.example.ralphcarneiro.qrcode.model.Usuario;
import com.example.ralphcarneiro.qrcode.service.APIService;
import com.example.ralphcarneiro.qrcode.service.EreecRetrofitServiceConfig;
import com.example.ralphcarneiro.qrcode.util.MacUtil;
import com.example.ralphcarneiro.qrcode.util.NetWorkUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    @Bind(R.id.btn_login)
    protected Button btn_login;
    @Bind(R.id.campoLogin)
    protected EditText email;
    @Bind(R.id.campoSenha)
    protected EditText password;

    private AlertDialog alerta;
    private AlertDialog alertError;
    ProgressDialog progress;
    private Context context;

    APIService service;
    Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        context = getApplicationContext();

        btn_login.setOnClickListener(new AuthenticationOnClickListener());
    }

    private class AuthenticationOnClickListener implements View.OnClickListener{

        @Override
        public void onClick(final View view) {
            if(!NetWorkUtil.isConnected(context)){

                showDialog(context.getString(R.string.erro_conexao), context.getString(R.string.message_dialog_erro_conection), true, false);
            }else{
                try {

                    final String email_ = email.getText().toString();
                    final String password_ = password.getText().toString();
                    if(!email_.trim().equals("") && !password_.trim().equals("")){

                        final Autenticar autenticar = new Autenticar(email.getText().toString(),password.getText().toString());

                        if(service == null)
                            service = EreecRetrofitServiceConfig.getServiceConfig();


                        //Login.this.runOnUiThread(new Runnable() {
                            //public void run() {
                        progress = ProgressDialog.show(Login.this, "Autenticando", getString(R.string.message_dialog_autentication), true);
                            //}
                        //});

                        String mac = MacUtil.getMacAddr((WifiManager)  getApplicationContext().getSystemService(Context.WIFI_SERVICE));
                        Call<Usuario> login = service.login(mac, autenticar);

                        login.enqueue(new Callback<Usuario>() {
                            @Override
                            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                                Usuario usuario = response.body();

                                if(usuario != null){
                                    if(usuario.getSuccess()){
                                        Log.i("Login", usuario.getNome() +" | "+ usuario.getEmail());
                                        Intent intent = new Intent(getApplicationContext(),EreecMainActivity.class);
                                        intent.putExtra("usuario", usuario);
                                        //if (progress != null)
                                        progress.dismiss();
                                        startActivity(intent);
                                        finish();
                                    }else{
                                        try {
                                            //if (progress != null)
                                            progress.dismiss();

                                            showDialogStartMessageLoginError();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }else{
                                    progress.dismiss();
                                    showDialogStartMessageLoginError();
                                }
                            }

                            @Override
                            public void onFailure(Call<Usuario> call, Throwable t) {
                                Log.i("Login" , t.getMessage());
                                //if (progress != null)
                                progress.dismiss();
                                showDialogStartMessageLoginError();
                            }
                        });






                    }else{
                        showDialogStartMessageLoginError();
                    }

                }catch (Exception e){

                }
            }
        }
    }

    public void showDialog(String message, String messageBody, boolean visible, final boolean destroy){
        LayoutInflater li = getLayoutInflater();


        View view = li.inflate(R.layout.dialog_msg, null);

        view.findViewById(R.id.bt).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                alerta.dismiss();
                if(destroy){
                    finish();
                }
            }
        });

        TextView t = (TextView) view.findViewById(R.id.message_dialog);
        t.setText(message);
        TextView message_body = (TextView) view.findViewById(R.id.message_body);
        if(visible)
            message_body.setVisibility(View.VISIBLE);
        message_body.setText(messageBody);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setView(view);

        alerta = builder.create();
        alerta.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alerta.setCanceledOnTouchOutside(false);

        alerta.show();


    }


    public void  showDialogStartMessageLoginError(){

        runOnUiThread(new Runnable(){
            @Override
            public void run(){
                LayoutInflater li = getLayoutInflater();


                View view = li.inflate(R.layout.dialog_login_error, null);
                Button btn = (Button) view.findViewById(R.id.btn_error);
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertError.dismiss();
                        alertError.cancel();

                    }
                });

                AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);

                builder.setView(view);

                alertError = builder.create();
                alertError.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                alertError.show();
            }
        });


    }

    public void alerta(Activity activity) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Autenticação");
        builder.setMessage("Não foi possivel se autenticar com essas credencias!");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        builder.show();
    }
}