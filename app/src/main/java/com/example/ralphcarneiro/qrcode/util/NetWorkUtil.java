package com.example.ralphcarneiro.qrcode.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * Created by Geferson on 10/07/16.
 */
public class NetWorkUtil {

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 3;
    public static NetworkInfo getNetworkInfo(Context context){

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo();

    }



    /**

     * Check if there is any connectivity

     * @param context

     * @return

     */

    public static boolean isConnected(Context context){

        NetworkInfo info = NetWorkUtil.getNetworkInfo(context);

        return (info != null && info.isConnected());

    }





    public static boolean isConnectedWifi(Context context){

        NetworkInfo info = NetWorkUtil.getNetworkInfo(context);

        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);

    }





    public static boolean isConnectedMobile(Context context){

        NetworkInfo info = NetWorkUtil.getNetworkInfo(context);

        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);

    }



    /**

     * Check if there is fast connectivity

     * @param context

     * @return

     */

    public static boolean isConnectedFast(Context context){

        NetworkInfo info = NetWorkUtil.getNetworkInfo(context);

        return (info != null && info.isConnected() && NetWorkUtil.isConnectionFast(info.getType(),info.getSubtype()));

    }



    /**

     * Check if the connection is fast

     * @param type

     * @param subType

     * @return

     */

    public static boolean isConnectionFast(int type, int subType){

        if(type==ConnectivityManager.TYPE_WIFI){

            return true;

        }else if(type==ConnectivityManager.TYPE_MOBILE){

            switch(subType){

                case TelephonyManager.NETWORK_TYPE_1xRTT:

                    return false; // ~ 50-100 kbps

                case TelephonyManager.NETWORK_TYPE_CDMA:

                    return false; // ~ 14-64 kbps

                case TelephonyManager.NETWORK_TYPE_EDGE:

                    return false; // ~ 50-100 kbps

                case TelephonyManager.NETWORK_TYPE_EVDO_0:

                    return true; // ~ 400-1000 kbps

                case TelephonyManager.NETWORK_TYPE_EVDO_A:

                    return true; // ~ 600-1400 kbps

                case TelephonyManager.NETWORK_TYPE_GPRS:

                    return false; // ~ 100 kbps

                case TelephonyManager.NETWORK_TYPE_HSDPA:

                    return true; // ~ 2-14 Mbps

                case TelephonyManager.NETWORK_TYPE_HSPA:

                    return true; // ~ 700-1700 kbps

                case TelephonyManager.NETWORK_TYPE_HSUPA:

                    return true; // ~ 1-23 Mbps

                case TelephonyManager.NETWORK_TYPE_UMTS:

                    return true; // ~ 400-7000 kbps

			/*

			 * Above API level 7, make sure to set android:targetSdkVersion

			 * to appropriate level to use these

			 */

                case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11

                    return true; // ~ 1-2 Mbps

                case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9

                    return true; // ~ 5 Mbps

                case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13

                    return true; // ~ 10-20 Mbps

                case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8

                    return false; // ~25 kbps

                case TelephonyManager.NETWORK_TYPE_LTE: // API level 11

                    return true; // ~ 10+ Mbps

                // Unknown

                case TelephonyManager.NETWORK_TYPE_UNKNOWN:

                default:

                    return false;

            }

        }else{

            return false;

        }

    }
    /*public static int getConnectivityType(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            int type = TYPE_NOT_CONNECTED;
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                type = TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                type = TYPE_MOBILE;

            if (isActive(activeNetwork)) {
                return type;
            } else {
                return TYPE_NOT_CONNECTED;
            }
        }
        return TYPE_NOT_CONNECTED;
    }

    private static boolean isActive(NetworkInfo activeNetwork) {
        return activeNetwork.isConnected() && activeNetwork.isAvailable();
    }

    public static boolean isConnected(Context context) {
        if (getConnectivityType(context) != TYPE_NOT_CONNECTED) {
            //Thread para poder acessar a internet.
            AsyncTask<Void, Void, Boolean> hasNetworkTask = new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected Boolean doInBackground(Void... params) {
                    return hasActiveInternetConnection();
                }
            };

            try {
                return hasNetworkTask.execute().get();
            } catch (InterruptedException e) {
                Log.e("InterruptedException", e.getMessage());
                return false;
            } catch (ExecutionException e) {

                Log.e("ExectionException", e.getMessage());
                return false;
            }
        }
        return false;
    }

    //Checa se consegue se conectar a um site externo.
    public static boolean hasActiveInternetConnection() {
        try {
            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(1500);
            urlc.connect();

            return (urlc.getResponseCode() == 200);
        } catch (IOException e) {
            Log.e(e.getMessage(), "Error checking internet connection");
        }
        return false;
    }

    public static String getConnectivityTypeString(Context context) {
        int conn = NetWorkUtil.getConnectivityType(context);
        String status = null;
        if (conn == NetWorkUtil.TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == NetWorkUtil.TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == NetWorkUtil.TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
        }
        return status;
    }*/
}
