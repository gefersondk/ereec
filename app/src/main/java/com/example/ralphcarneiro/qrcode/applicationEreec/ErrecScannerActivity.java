package com.example.ralphcarneiro.qrcode.applicationEreec;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.example.ralphcarneiro.qrcode.R;
import com.example.ralphcarneiro.qrcode.util.EreecConstants;
import com.google.zxing.Result;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static com.example.ralphcarneiro.qrcode.util.EreecConstants.PERMISSIONS_REQUEST_ACCESS_CAMERA;

/**
 * Created by gefersondias on 04/06/17.
 */

public class ErrecScannerActivity extends Activity implements ZXingScannerView.ResultHandler {

    @Bind(R.id.zxscan)
    protected ZXingScannerView mScannerView;
    @Bind(R.id.btn_cancel_scan)
    protected Button btn_cancel_scan;

    private final String TAG = "VERIFICANDO SCANNER";

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.scan_layout);


//        mScannerView = (ZXingScannerView) findViewById(R.id.zxscan);//new ZXingScannerView(this);
        ButterKnife.bind(this);
        //setContentView(mScannerView);
        btn_cancel_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mScannerView.stopCamera();
                setResult(EreecConstants.RESULT_CONFIRM_CHECKIN_CANCELL);
                finish();
            }
        });



    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                this.checkSelfPermission(Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA},
                    PERMISSIONS_REQUEST_ACCESS_CAMERA);
        } else {
            mScannerView.startCamera();
        }

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        mScannerView.stopCamera();
        setResult(EreecConstants.RESULT_CONFIRM_CHECKIN_CANCELL);
        finish();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void onStop() {
        super.onStop();
        mScannerView.stopCamera();
        finish();
    }

    @Override
    public void handleResult(Result rawResult) {

        Intent intent = new Intent();
        intent.putExtra("SCAN_RESULT", rawResult.getText());

        setResult(EreecConstants.RESULT_SCAN,intent);

        mScannerView.stopCamera();
        finish();
        // If you would like to resume scanning, call this method below:
        //mScannerView.resumeCameraPreview(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSIONS_REQUEST_ACCESS_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mScannerView.startCamera();
            }
        }
    }
}
