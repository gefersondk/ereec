package com.example.ralphcarneiro.qrcode.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ralph Carneiro on 30/05/2017.
 */

public class Minicurso implements Parcelable{

    @SerializedName("id")
    private long id;
    @SerializedName("nome")
    private String nomeMinicurso;
    @SerializedName("palestrante")
    private String nomePalestrante;
    @SerializedName("descricao")
    private String descricaoMinucurso;
    @SerializedName("data")
    private String date;

    public Minicurso(){}

    public Minicurso(long id, String nomeMinicurso, String nomePalestrante, String descricaoMinucurso, String date) {
        this.id = id;
        this.nomeMinicurso = nomeMinicurso;
        this.nomePalestrante = nomePalestrante;
        this.descricaoMinucurso = descricaoMinucurso;
        this.date = date;
    }

    protected Minicurso(Parcel in) {
        id = in.readLong();
        nomeMinicurso = in.readString();
        nomePalestrante = in.readString();
        descricaoMinucurso = in.readString();
        date = in.readString();
    }

    public static final Creator<Minicurso> CREATOR = new Creator<Minicurso>() {
        @Override
        public Minicurso createFromParcel(Parcel in) {
            return new Minicurso(in);
        }

        @Override
        public Minicurso[] newArray(int size) {
            return new Minicurso[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNomeMinicurso() {
        return nomeMinicurso;
    }

    public String getNomePalestrante() {
        return nomePalestrante;
    }

    public String getDate() {
        return this.date;
    }

    public void setNomeMinicurso(String nomeMinicurso) {
        this.nomeMinicurso = nomeMinicurso;
    }

    public void setNomePalestrante(String nomePalestrante) {
        this.nomePalestrante = nomePalestrante;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescricaoMinucurso() {
        return descricaoMinucurso;
    }

    public void setDescricaoMinucurso(String descricaoMinucurso) {
        this.descricaoMinucurso = descricaoMinucurso;
    }

    @Override
    public String toString() {
        return "Minicurso{" +
                ", nomeMinicurso='" + nomeMinicurso + '\'' +
                ", nomePalestrante='" + nomePalestrante + '\'' +
                ", date=" + date +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(nomeMinicurso);
        dest.writeString(nomePalestrante);
        dest.writeString(descricaoMinucurso);
        dest.writeString(date);
    }
}
