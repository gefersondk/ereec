package com.example.ralphcarneiro.qrcode.EreecFragment;

import android.support.v4.app.Fragment;

/**
 * Created by gefersondias on 03/06/17.
 */

public enum EreecFragment {

    palestras {
        @Override
        public Fragment getEreecFragment() {
            return new EreecFragmentPalestras();
        }
    }, minicursos {
        @Override
        public Fragment getEreecFragment() {
            return new EreecFragmentMinicursos();
        }
    };

    public abstract Fragment getEreecFragment();
}
